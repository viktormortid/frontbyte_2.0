$(document).ready(function () {
    var $slickElement = $('.js-slider-inside');

    $slickElement.slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        swipe: false,
        dotsClass: 'slick-dots b-slider__dots',
        responsive: [{
                breakpoint: 1180,
                settings: {
                    infinite: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    dots: true,
                    adaptiveHeight: true,
                    swipe: true,
                }
            },
            {
            breakpoint: 768,
            settings: {
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: false,
                dots: false,
                adaptiveHeight: true,
                swipe: true,
                nextArrow: $slickElement.parent().find('.js-next'),
                prevArrow: $slickElement.parent().find('.js-prev'),
            }
        }]
    });

    $('.b-nav__btn-menu').on('click', function () {
        if ($('.b-nav').hasClass('open')) {
            $('.b-nav').removeClass('open')
            $('.b-logo').removeClass('b-logo--dark')
        }
        else {
            $('.b-nav').addClass('open');
            $('.b-logo').addClass('b-logo--dark')
        }
        
    });
});